**Дипломный проект. Инфраструктура. ПО.<br />**

Репозиторий содержит скрипты Terraform и Ansible для развертывания инфраструктуры и необходимого ПО в каталогах 1_terraform и 2_ansible соответственно. <br />
А также helm-чарт деплойментов базы и приложения в каталоге 3_helm.<br/>Для использования СI/CD необходимо наличие настроенного репозитория GitLab с включенными GitLab Pages (Settings-Pages), которая необходима для публикации helm-чарта в репозиторий artifacthub.io).<br/>
Поддерживается только Yandex Cloud. Для обхода проблемы с блокировкой доступа к hashicorp можно использовать зеркало яндекс. 
Для этого в домашнем каталоге пользователя под которым запускается terraform  создать файл .terraformrc со следующим содержимым:<br />
provider_installation { <br />
  network_mirror { <br />
    url = "https://terraform-mirror.yandexcloud.net/" <br />
    include = ["registry.terraform.io/*/*"] <br />
  } <br />
  direct { <br />
    exclude = ["registry.terraform.io/*/*"] <br />
  } <br />
<br />
Перед установкой необходимо создать <a href="https://cloud.yandex.ru/docs/billing/quickstart/#create_billing_account">платежный аккаунт Яндекс</a>, <a href="https://cloud.yandex.ru/docs/resource-manager/operations/folder/create">каталог</a> и <a href="https://cloud.yandex.ru/docs/iam/operations/sa/create">сервисный аккаунт</a>.
Для сервисного аккаунта необходимо <a href="https://cloud.yandex.ru/docs/cli/operations/authentication/service-account">создать авторизованный ключ</a> и сохранить его в файле на компьютере с которого будет выполняться установка.<br/>
1. Развертывание инфраструктуры<br/>
В файле terraform.tfvars в каталоге 1_terraform необходимо в переменных cloud_id и folder_id указать свои идентификаторы облака и каталога Яндекс. Переопределить остальные параметры или оставить как есть.<br/>
В файле main.tf в параметре service_account_key_file задать путь к файлу с ключом к своему сервисному аккаунту (см выше).<br/>
В файле main.tf в параметре users в блоках vm-1, vm-2 и vm-3 указать путь к файлу с параметрами пользователя, который будет автоматически создан на серверах.<br/>
Формат файла следующий (см более подробное <a href="">описание<a/>):<br/>
users:<br/>
  \- name: ansible<br/>
    groups: sudo<br/>
    shell: /bin/bash<br/>
    sudo: ['ALL=(ALL) NOPASSWD:ALL']<br/>
    ssh-authorized-keys:<br/>
      \- ssh-rsa AAAAB3...N7Mqa+ ansible<br/><br/>
Где в ssh-authorized-keys указывается публичный ключ, с которым которым будет выполняться подключение. Создается при генерации открытого и закрытого ключей на компьютере, с которого выполняется установка.<br/>
После этого можно приступить к развертыванию инфраструктуры выполнив последовательно команды:<br/>
terraform init<br/>
terraform plan<br/>
terraform apply<br/>
Результатом выполнения будет создание в вашем облаке трех серверов (sf-master, sf-app, sf-srv), сети для них, сетевого балансировщика, а также inventory-файла (2_ansible/inventory/inventory.ini) для ansible скриптов, при помощи которых будет устанавливаться необходимое ПО и выполняться настройки.<br/>
Согласно задания, sf-master - управляющая нода кластера кубернетес, sf-app - рабочая нода кластера и sf-srv - сервер мониторинга.
<br/><br/>
2. Подготовка к установке ПО<br/>
В файле devops-diplom.yml из каталога 2_ansible/inventory в параметре gitlab_runner_registration_token указать токен runner'а для выполнения CICD заданий. Токен можно увидеть настройках вашего репозитория GitLab (Settings-CI/CD-Runners). Также необходимо отключить shared runners так как они нам в данном задании не нужны.<br/>
В файле 2_ansible/inventory/devops-diplom-vars.yml в указываются параметры используемые при установке helm-чарта (неймспейс, наименование деплоя, чарта, а также репозиторий чарта.) В данном проекте используется репозиторий https://lexr7.gitlab.io/DEVOPS_Diplom_infra/ но можно создать свой, при этом выполнив необходимые настройки репозитория artifacthub.io.<br/>
Все прочие параметры содержат предустановленые значения, включая основной inventory-файл, который создается в процессе работы terraform при выполнении пункта выше.<br/><br/>
3. Установка ПО<br/>
Из каталога 2_ansible запускается ansible-плейбук devops-diplom.yml командой:<br/>
ansible-playbook -u ansible -i inventory/inventory.ini devops-diplom.yml -b <br/>
В процессе работы плейбука выполняется установка docker на все хосты, установка кластера kubernetes на управляющую ноду и рабочую ноду (используя kubespray), установку gitlab-runner на управляющую ноду, и установку helm-чарта с приложением.<br/>
Сам helm-чарт находится в каталоге 3_helm/source.<br/>
В файле artifacthub-repo.yml в каталоге 3_helm указывается идентификатор репозитория (repositoryID) и имя владельца (owners).<br/>
При внесении изменений в чарт и необходимости их публикации в репозиторий в каталоге 3_helm выполняются команды:<br/>
helm package source<br/>
helm repo index . <br/>
После комммита изменений автоматически запускается конвейер CI/CD который выполнит публикацию содержимого каталога на страницу GitLab Pages откуда изменения будет автоматически загружены в репозиторий artifacthub.io (в настройках которого должен быть задан url страницы репозитория Gitlab).<br/><br/>
4. Проверка
После выполнения всех действий необходимо зайти на управляющую ноду (sf-master) и выполнив команды:<br/>
kubectl cluster-info<br/>
kubectl get deploy -n devops<br/>
убедиться, что кластер поднят и приложения запущены. Также можно зайти в браузере по адресу созданного сетевого балансировщика и убедиться, что интерфейс приложения открывается.<br/><br/>
Описание процесса изменения приложения и запуска СI/CD по доставке изменений в кластер на странице проекта <a href="https://gitlab.com/LexR7/DEVOPS_Diplom_app">DEVOPS_Diplom_app</a>.<br/>
Описание настройки монитороринга на странице проекат <a href="https://gitlab.com/LexR7/devops_diplom_monitoring">DEVOPS_Diplom_monitoring</a>.
